<?php

// использовать так
//if ( have_posts() ) {
//    while ( have_posts() ) {
//        the_post();
//        echo '<div class="query">';
//        show_script();
//        echo ajax_query();
//        echo '</div>';
//
//        echo '<div class="pagination">';
//        echo ajax_pagination();
//        echo '</div>';
//    }
//}


function show_script() {
    ?>
    <script>
        let ajaxUrl = '<?php echo admin_url( 'admin-ajax.php' ) ?>';
        let nonce = '<?php echo wp_create_nonce( "nonce" ) ?>';

        (function ($) {
            $(document).ready(function () {
                $(document).on('click touch keypress', 'button.btn', function (e) {
                    e.preventDefault();

                    let queryWrap = $('.query');
                    let paginationWrap = $('.pagination');
                    let page = $(this).data('page');

                    queryWrap.addClass('current');

                    if (!$(this).hasClass('active')) {
                        $.ajax({
                            url: ajaxUrl,
                            type: "POST",
                            data: {
                                action: 'update_pagination',
                                page: page,
                                nonce: nonce,
                            },
                            success: function (data) {
                                paginationWrap.html('');
                                paginationWrap.html(data.pagination);

                                queryWrap.html('');
                                queryWrap.html(data.query);
                            },
                        });
                    }
                });
            });
        })(jQuery);
    </script>
    <?php
}


function ajax_query( int $current_page = 1 ) {
    $html = '';

    $posts_studies_arg = [
        'post_type'      => 'post',
        'post_status'    => 'publish',
        'paged'          => $current_page,
        'posts_per_page' => 5,
        'order'          => 'DESC',
        'orderby'        => 'date',
    ];

    $query = get_posts( $posts_studies_arg );

    foreach ( $query as $idx => $case ) {
        $id = $case->ID;
        $permalink = get_permalink( $id );
        $title = get_the_title( $id );

        $html .= "<div>";
        $html .= "<a href='{$permalink}'>";
        $html .= "<h2>{$title}</h2>";
        $html .= "</a>";
        $html .= "</div>";
    }

    return $html;
}

function ajax_pagination( int $current_page = 1 ) {
    $total_posts = wp_count_posts( 'post' )->publish;
    $case_per_page = 5;
    $total_pages = (int)ceil( $total_posts / $case_per_page );
    $end_size = 1;
    $mid_size = 2;

    if ( $total_pages <= 1 ) {
        return null;
    }

    $links_arr = [];
    $pages_pag = '';
    $dots = false;

    for ( $i = 1; $i <= $total_pages; $i++ ) {
        if ( $i === $current_page ) {
            $links_arr[] = "<button class='btn active'>{$i}</button>";
            $dots = true;
        } else {
            if ( $i <= $end_size ||
                ( $current_page && $i >= $current_page - $mid_size && $i <= $current_page + $mid_size ) ||
                $i > $total_pages - $end_size ) {
                $links_arr[] = "<button class='btn' data-page='{$i}'>{$i}</button>";
                $dots = true;
            } elseif ( $dots ) {
                $links_arr[] = "<span class='more'></span>";
                $dots = false;
            }
        }
    }

    foreach ( $links_arr as $item ) {
        $pages_pag .= $item;
    }

    return $pages_pag;
}

add_action( 'wp_ajax_update_pagination', 'update_pagination' );
add_action( 'wp_ajax_nopriv_update_pagination', 'update_pagination' );

function update_pagination() {
    $start = microtime( true );
    $nonce = $_POST['nonce'] ?? null;

    if ( !wp_verify_nonce( $nonce, 'nonce' ) ) {
        wp_die();
    }

    $page = (int)( $_POST['page'] ?? null );

    if ( !$page ) {
        wp_die();
    }

    $new_pagination = ajax_pagination( $page );
    $new_query = ajax_query( $page );
    $end_time = round( microtime( true ) - $start, 4 );

    wp_send_json( [
        'query'      => $new_query,
        'pagination' => $new_pagination,
        'time'       => $end_time,
    ], 200
    );
}
